package areaEperimetro;

public class Losango {
    
    private static double diagonalMaior;
    private static double diagonalMenor;
    private static double lado;

    public Losango(double diagonalMaior, double diagonalMenor, double lado) {
        this.diagonalMaior = diagonalMaior;
        this.diagonalMenor = diagonalMenor;
        this.lado = lado;
    }

    public Losango() {
    }
    

    public double getDiagonalMaior() {
        return diagonalMaior;
    }

    public void setDiagonalMaior(double diagonalMaior) {
        this.diagonalMaior = diagonalMaior;
    }

    public double getDiagonalMenor() {
        return diagonalMenor;
    }

    public void setDiagonalMenor(double diagonalMenor) {
        this.diagonalMenor = diagonalMenor;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    
    
    public static double area(){
        return (diagonalMaior * diagonalMenor) / 2; 
    }
    
    public static double perimetro(){
        return 4 * lado;
    }
}
