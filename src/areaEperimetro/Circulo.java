package areaEperimetro;

public class Circulo {
    
    private static double raio;
    private static double diametro;
    private static double pi = 3.14;

    public Circulo(double raio, double diametro) {
        if (raio == 0 && diametro != 0){
            this.raio = diametro / 2;
        }else{
            this.raio = raio;
        }
        this.diametro = diametro;
    }

    public Circulo() {
     
    }

    
    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }

    public static double getDiametro() {
        return diametro;
    }

    public static void setDiametro(double diametro) {
        Circulo.diametro = diametro;
        Circulo.raio = diametro / 2; 
    }

    
    public static double area(){
        
        return pi * (raio * raio);
    }
    
    private static double perimetro(){
        
        return 2 * pi * raio;
    }
}
