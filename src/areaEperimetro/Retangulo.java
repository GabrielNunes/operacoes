package areaEperimetro;

public class Retangulo {
    
    private static double base;
    private static double altura;

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public Retangulo() {
    }
    

    public static double getBase() {
        return base;
    }

    public static void setBase(double base) {
        Retangulo.base = base;
    }

    public static double getAltura() {
        return altura;
    }

    public static void setAltura(double altura) {
        Retangulo.altura = altura;
    }
    
    
    public static double area(){
        return base * altura;
    }
    
    public static double perimetro(){
        return 2 * (base + altura);
    }
}
