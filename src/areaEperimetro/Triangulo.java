package areaEperimetro;

public class Triangulo {
    
    private static double altura;
    private static double base; 
    private static double ladoDir; 
    private static double ladoEsq; 

    public Triangulo(double altura, double base, double ladoDir, double ladoEsq) {
        this.altura = altura;
        this.base = base;
        this.ladoDir = ladoDir;
        this.ladoEsq = ladoEsq;
    }

    public Triangulo() {
    }
    

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getLadoDir() {
        return ladoDir;
    }

    public void setLadoDir(double ladoDir) {
        this.ladoDir = ladoDir;
    }

    public double getLadoEsq() {
        return ladoEsq;
    }

    public void setLadoEsq(double ladoEsq) {
        this.ladoEsq = ladoEsq;
    }
    
    
    public static double area(){
        return (base * altura) / 2;
    }
    
    public static double perimetro(){
        return base + ladoDir + ladoEsq;
    }
}
