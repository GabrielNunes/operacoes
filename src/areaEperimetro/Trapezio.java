package areaEperimetro;

public class Trapezio {
    
    private static double baseMaior;
    private static double baseMenor;
    private static double altura;
    private static double lado1;
    private static double lado2;

    public Trapezio(double baseMaior, double baseMenor, double altura, double lado1, double lado2) {
        this.baseMaior = baseMaior;
        this.baseMenor = baseMenor;
        this.altura = altura;
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    public Trapezio() {
    }
    

    public static double getBaseMaior() {
        return baseMaior;
    }

    public static void setBaseMaior(double baseMaior) {
        Trapezio.baseMaior = baseMaior;
    }

    public static double getBaseMenor() {
        return baseMenor;
    }

    public static void setBaseMenor(double baseMenor) {
        Trapezio.baseMenor = baseMenor;
    }

    public static double getAltura() {
        return altura;
    }

    public static void setAltura(double altura) {
        Trapezio.altura = altura;
    }

    public static double getLado1() {
        return lado1;
    }

    public static void setLado1(double lado1) {
        Trapezio.lado1 = lado1;
    }

    public static double getLado2() {
        return lado2;
    }

    public static void setLado2(double lado2) {
        Trapezio.lado2 = lado2;
    }
    
    
    public static double area(){
        return ((baseMaior + baseMenor) * altura) / 2;
    }
    
    public static double perimetro(){
        return baseMaior + baseMenor + lado1 + lado2;
    }
    
}
