package areaEperimetro;

public class Quadrado {
    
    private static double lado;

    public Quadrado(double lado) {
        this.lado = lado;
    }

    public Quadrado() {
    }
    

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    
    
    public static double area(){
        
        return lado * lado;
    }
    
    public static double perimetro(){
        
        return 4 * lado;
    }
    
}
