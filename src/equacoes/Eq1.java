package equacoes;

public class Eq1 {
    
    private static double a;
    private static double b;

    public Eq1(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public static double getA() {
        return a;
    }

    public static void setA(double a) {
        Eq1.a = a;
    }

    public static double getB() {
        return b;
    }

    public static void setB(double b) {
        Eq1.b = b;
    }
    

    public static double x(){
        
        return (b * (-1)) / a;
    }
    
}
