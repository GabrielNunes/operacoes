package funcoes;

public class Func1 {
    
    private static double a;
    private static double b;

    public Func1(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Func1() {
    }
    

    public static double getA() {
        return a;
    }

    public static void setA(double a) {
        Func1.a = a;
    }

    public static double getB() {
        return b;
    }

    public static void setB(double b) {
        Func1.b = b;
    }
    
    
    public static double x(){
        
        return (b * (-1)) / a;
    }
    
    public static double cortaY(){
        return b;
    }
}
