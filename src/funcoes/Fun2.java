package funcoes;

public class Fun2 {
    
    private static double a;
    private static double b;
    private static double c;

    public Fun2(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Fun2() {
    }
    

    public static double getA() {
        return a;
    }

    public static void setA(double a) {
        Fun2.a = a;
    }

    public static double getB() {
        return b;
    }

    public static void setB(double b) {
        Fun2.b = b;
    }

    public static double getC() {
        return c;
    }

    public static void setC(double c) {
        Fun2.c = c;
    }
    
    
    public static double xUm(){
        
        return (- b + Math.sqrt((b * b) - (4 * a * c))) / (2 * a);
    }
    
     public static double xDois(){
        
        return (- b - Math.sqrt((b * b) - (4 * a * c))) / (2 * a);
    }
     
    public static double vectorX(){
        
        return (- b) / (2 * a);
    }
    
    public static double vectorY(){
        
        double delta = (b * b) - (4 * a * c);
        
        return (- delta) / (4 * a);
    }
    
    public static double cortaY(){
        return b;
    }
}
